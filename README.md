# About
This is a semi-classless SASS library, meaning that minimal changes to HTML are required.
HTML elements are styled as-is where possible to avoid redundancies from other libraries such as having to assign your `<button>`s a `button` class.
It also encourages the use of semantic HTML elements and provides styles for other page items such as messages and grids.

## Usage
Either include `index.sass` in an existing project or install `sassc` and run `make` to compile CSS in the `build/` directory
